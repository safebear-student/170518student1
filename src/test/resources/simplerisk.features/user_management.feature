Feature: User Management

  User Story:
  In order to mange users
  s a administrator
  I need CRUD permissions to user accounts

  Rules
  -Non Admin usres hae no permissions over other accounts
  --Risk managers
  --Asset managers
  -Non-Admin users must only be able t see riscka or assetts in ther groups
  - the admin must be able to reset oter users passwords
  - When an acct is created, a user must be forced to reste their password on first logon

  Questions;
   - Do admin users neeeed to access all accounts or only those in their group

  To do:
  - Force reste of password on account creation

  Domain Language:
  Group = Users are dedefined by which Group they belong to
  CRUD = Create Read Update Delete
  admin permissions = acess to CRUD risks, usre and assets for all groups
  risk_management permissions = Only able to CRUD risks
  asset_management permissions = Only able to CRUD assets

  Background:
    Given a user called simon with administrator permissions and password S@feB3ar
    And a user called tom with risk_management permissions and password S@feB3ar

    @high-impact
    Scenario Outline: he administrator checksa users' details
      When simon is logged in with password S@feB3ar
      Then he is able to view <user>'s account
      Examples:
        | user |
        |tom   |

      @high-risk
      @to-do
        Scenario Outline: A users' password is reset by teh administrator
        Given a <user> has lost his password
        When simon is logged in with password S@feB3ar
        Then simon can reset <user>'s password
        Examples:
          | user |
          |tom   |
