package com.bdd.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 06/12/2017.
 */
public class BasePage {

    WebDriver driver;

    //Find the logout link
    @FindBy(xpath = "//*[@class = \"dropdown-toggle\"]")
    WebElement user_menu;
    @FindBy(linkText = "Logout")
    WebElement logout_link;

    //Find the ribbon menu options
    @FindBy(linkText = "Configure")
    WebElement configure_link;

    // Logout
    public void logout(){

        user_menu.click();
        logout_link.click();

    }

    // The methods for clicking on the ribbon menus
    public void clickConfigureLink(){
        configure_link.click();
    }


}
